var HTTPSMODULE, RECORDMODULE, SEARCHMODULE, URLMODULE, ITEMRECEIPTMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/https', 'N/record', 'N/search', 'N/url', '/SuiteScripts/bwpScripts/Modules/itemreceiptModule'], runUserEvent);

function runUserEvent(https, record, search, url, itemreceiptModule) {
	HTTPSMODULE= https;
	RECORDMODULE= record;
	SEARCHMODULE= search;
	URLMODULE= url;
	ITEMRECEIPTMODULE= itemreceiptModule;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	logVar('Scripte Type',scriptContext.type);
	if (scriptContext.type == 'create'){
		// Code is disabled, it was suggested by SuiteAnswers 74802 but calling a Suitelet may result in a time out
//		// call suitelet
//		var suiteletUrl = URLMODULE.resolveScript({
//			scriptId: 'customscript_bwp_sl_irfromif',
//			deploymentId: 'customdeploy_bwp_sl_irfromif',
//			returnExternalUrl: true,
//			params: {
//				custparam_bwp_itemfulfillmentid: scriptContext.newRecord.id
//			}
//		});
//		var response = HTTPSMODULE.get({
//			url: suiteletUrl
//		});
//		log.debug({title: 'Suitelet response', details: response});

		var itemReceiptId = createItemReceipt(scriptContext);
		
		if (itemReceiptId) {
			
			itemReceiptRecord = RECORDMODULE.load({
				type: RECORDMODULE.Type.ITEM_RECEIPT,
				id: itemReceiptId,
				isDynamic: false
			});

			ITEMRECEIPTMODULE.processItemReceipt(itemReceiptRecord, 'create');
		}
		
	}
	log.debug('afterSubmit done');
}

function createItemReceipt(scriptContext){
	log.debug({title: 'createItemReceipt', details: 'Started'});
		
	var itemFulFillmentRecord = scriptContext.newRecord;
		
	var itemFulFillmentDate = itemFulFillmentRecord.getValue('trandate'); // 19/10/21
	var itemFulFillmentMemo = itemFulFillmentRecord.getValue('memo');	// 29/11/21
	
	var salesOrderId = itemFulFillmentRecord.getValue({
		 fieldId: 'createdfrom'
	 });
	
	try {
		var salesOrderRecord = RECORDMODULE.load({
			type: RECORDMODULE.Type.SALES_ORDER,
			id: salesOrderId,
			isDynamic: false,		
		});
	} catch (error) {
		log.debug('errorMessage', error.message);
		return;
	}
	
	var purchaseOrderId = salesOrderRecord.getValue({
		 fieldId: 'intercotransaction'
	 });
	
	if (!purchaseOrderId){
		return;
	}
	
	// Objet cible
//	{
//		soLine: '',
//		sopoLineIndex: '',
//		poLine: '',
//		item: '',
//		quantityToReceive: ''
//	}
//	var ifLines = { ifLines: [] };
	var ifLines = [];
	//1. boucler sur if, initialiser l'object (notamment avec la valeur du champ if.orderline) pour les if avec une quantité <> 0
	retrieveItemFulfillmentLineInfo(itemFulFillmentRecord, ifLines);
	//2. boucler sur les lignes de so, pour chaque, rechercher si elle existe dans l'objet, si oui renseigner le numéro d'index de la ligne de SO
	retrieveSalesOrderLineInfo(salesOrderRecord, ifLines);
	//3. boucler sur l'objet lire les ligne de PO sur la base de l'index, renseigner la valeur de po.line correspondant
	retrievePurchaseOrderLineInfo(purchaseOrderId, ifLines);
	
	//4. pour chaque ligne de ir, rechercher dans l'objet avec objet.poline = ir.orderline ==> si n'existe pas : mettre itemreceive à false
	var itemReceiptRecord = RECORDMODULE.transform({
		 fromType: RECORDMODULE.Type.PURCHASE_ORDER,
		 fromId: purchaseOrderId,
		 toType: RECORDMODULE.Type.ITEM_RECEIPT,
		 isDynamic: true
	});
	
	itemReceiptRecord.setValue({
		fieldId: 'trandate',		//19/10/21
		value: itemFulFillmentDate
	});
	
	itemReceiptRecord.setValue({
		fieldId: 'memo',		//29/11/21
		value: itemFulFillmentMemo
	});
	
	var lineCountIR = itemReceiptRecord.getLineCount('item');    
	
    for (var counterIR = 0; counterIR < lineCountIR; counterIR++)
    {
    	itemReceiptRecord.selectLine({
    		sublistId: 'item',
    		line: counterIR
    	});
    	var irOrderLine = itemReceiptRecord.getCurrentSublistValue({
    		sublistId: 'item',
    		fieldId: 'orderline'
    	});
//    	var index = ifLines.ifLines.findIndex(function(element) { return element.poLine == irOrderLine;} );
    	var index = ifLines.findIndex(function(element) { return element.poLine == irOrderLine;} );
    	var quantityFromIF = 0;
    	
    	if (index >= 0){
//    		quantityFromIF = ifLines.ifLines[index].quantityToReceive;
    		quantityFromIF = ifLines[index].quantityToReceive;
			serialNoFromIF = ifLines[index].serialNo;
    	}
    	if (quantityFromIF > 0){			
    		itemReceiptRecord.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'quantity',
				value: quantityFromIF
			});    			
    	} else {
			itemReceiptRecord.setCurrentSublistValue({
        		sublistId: 'item',
        		fieldId: 'itemreceive',
        		value: false
        	}); 			 		
		}
		itemReceiptRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			value: serialNoFromIF
		});

    	itemReceiptRecord.commitLine({
    		sublistId: 'item'
    	}); 
    }   
    
    itemFulfillmentId = itemFulFillmentRecord.getValue({
    	fieldId: 'id'
    });
//    logRecord(itemReceiptRecord,'itemReceipt');
    itemReceiptRecord.setValue({
    	fieldId: 'custbody_bwp_item_fulfill_reference',
    	value: itemFulfillmentId
    });
    var itemReceiptId = itemReceiptRecord.save();    
    log.debug({ title:'item receipt Id created', details: itemReceiptId});
        
    
    // We need to call the IF to update the IR link
    var updateIFRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.ITEM_FULFILLMENT,
		id: itemFulfillmentId,
		isDynamic: true,		
	});	
    updateIFRecord.setValue({
    	fieldId: 'custbody_bwp_item_receipt_reference',
    	value: itemReceiptId
    });
    updateIFRecord.save();
	log.debug({title: 'createItemReceipt', details: 'Done'});
    
    return itemReceiptId;    
}

function retrieveItemFulfillmentLineInfo(itemFulFillmentRecord,ifLines){	
	var lineCountIF = itemFulFillmentRecord.getLineCount('item');

	for (var counterIF = 0; counterIF < lineCountIF; counterIF++)
	{	
		var ifOrderLine = itemFulFillmentRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'orderline',
			line: counterIF
		});
		
		var ifQuantityLine = itemFulFillmentRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'quantity',
			line: counterIF
		});
		
		var ifItemLine = itemFulFillmentRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'item',
			line: counterIF
		});

		var ifSerialNo = itemFulFillmentRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			line: counterIF
		});
		
//		ifLines.ifLines.push({item: ifItemLine, soLine: ifOrderLine, quantityToReceive: ifQuantityLine});	
		ifLines.push({item: ifItemLine, soLine: ifOrderLine, quantityToReceive: ifQuantityLine, serialNo: ifSerialNo});
	}
}

function retrieveSalesOrderLineInfo(salesOrderRecord,ifLines){	
	var lineCountSO = salesOrderRecord.getLineCount('item');
	
	for (var i = 0; i < lineCountSO; i++)
	{		
		var soLine = salesOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'line',
			line: i
		});		
//		var index = ifLines.ifLines.findIndex(function(element) { return element.soLine == soLine;} );
		var index = ifLines.findIndex(function(element) { return element.soLine == soLine;} );
		
		if (index >= 0){
//			ifLines.ifLines[index].sopoLineIndex = counterSO;	
			ifLines[index].sopoLineIndex = i;
		}
	}		
}

function retrievePurchaseOrderLineInfo(purchaseOrderId, ifLines){	
	var purchaseOrderRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.PURCHASE_ORDER,
		id: purchaseOrderId,
		isDynamic: false,		
	});	
	var lineCountPO = purchaseOrderRecord.getLineCount('item');
	
	// By construction PO lines are created from SO lines. So PO line at index i corresponds to the SO line with the same index 
	for (var i = 0; i < lineCountPO; i++)
	{	
		var linePO = purchaseOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'line',
			line: i
		});
		
//		var index = ifLines.ifLines.findIndex(function(element) { return element.sopoLineIndex == i;} );
		var index = ifLines.findIndex(function(element) { return element.sopoLineIndex == i;} );
		if (index >= 0){
//			ifLines.ifLines[index].poLine = linePO;		
			ifLines[index].poLine = linePO;
		}		
	}	
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}