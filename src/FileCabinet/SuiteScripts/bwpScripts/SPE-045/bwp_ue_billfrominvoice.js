var RECORDMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search'], runUserEvent);

function runUserEvent(record, search) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	
	if (scriptContext.type == 'create'){
		createBill(scriptContext);
	}
	log.debug('afterSubmit done');
}


function createBill(scriptContext){
	
	// var invoiceRecord = scriptContext.newRecord;
	var invoiceRecord = RECORDMODULE.load({
		type: scriptContext.newRecord.type,
		id: scriptContext.newRecord.id,
		isDynamic: false
	});
	
	var salesOrderId = invoiceRecord.getValue({
		 fieldId: 'createdfrom'
	 });
	
	var invoiceDate = invoiceRecord.getValue('trandate');	//19/10/21
	
	if (!salesOrderId) { return false; }
//	var salesOrderRecord = RECORDMODULE.load({
//		type: RECORDMODULE.Type.SALES_ORDER,
//		id: salesOrderId,
//		isDynamic: true,		
//	});
//	
//	var purchaseOrderId = salesOrderRecord.getValue({
//		 fieldId: 'intercotransaction'
//	 });
	
	var soFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.SALES_ORDER,
		id: salesOrderId,
		columns: ['intercotransaction']
	});
//	logRecord(soFieldsValues, 'soFieldsValues');
	var purchaseOrderId;
	if ('intercotransaction' in soFieldsValues) {
		purchaseOrderId = soFieldsValues.intercotransaction;
	}
	
	if (!purchaseOrderId){ return; }
	
//	var poRecord = RECORDMODULE.load({
//		type: RECORDMODULE.Type.PURCHASE_ORDER,
//		id: purchaseOrderId,
//		isDynamic: false,		
//	});
//	logRecord(poRecord, 'poRecord');
	var poFieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.PURCHASE_ORDER,
		id: purchaseOrderId,
		columns: ['tranid', 'statusref']
	});
//	logRecord(poFieldsValues, 'poFieldsValues');	
	if ('statusref' in poFieldsValues && poFieldsValues.statusref.length > 0) {
		var statusValue = poFieldsValues.statusref[0].value;
		var statusText = poFieldsValues.statusref[0].text;
		var tranid = poFieldsValues.tranid;
		if (statusValue == 'fullyBilled') {
			var msg = `Impossible to transform PO ${tranid} (${purchaseOrderId}) to vendor Bill. PO status is ${statusText} (${statusValue})`;
			log.debug({
				title: 'Invalid status',
				details: msg
			});
			return;
		}
	}
	
	var vendorBillRecord = RECORDMODULE.transform({
		 fromType: RECORDMODULE.Type.PURCHASE_ORDER,
		 fromId: purchaseOrderId,
		 toType: RECORDMODULE.Type.VENDOR_BILL,
		 isDynamic: true
	});
	
	// // get form used by the vendor bill
	// var customFormUsed = vendorBillRecord.getValue({
	// 	fieldId: 'customform'
	// });	
	// log.debug('custom form used', customFormUsed);

	// // set standard form on the current vendor bill
	// vendorBillRecord.setValue({
	// 	fieldId: 'customform',
	// 	value: 50	//correspond to standard form vendor bill
	// });

	vendorBillRecord.setValue({
		fieldId: 'trandate',		//19/10/21
		value: invoiceDate
	});
	
	// Eric Moulin - 24/03/2022 - Begin
	vendorBillRecord.setValue({
		fieldId: 'tranid',
		value: invoiceRecord.getValue({ fieldId: 'tranid' })
	});
	// Eric Moulin - 24/03/2022 - End

//	Objet cible
//	{
//		indexLine:
//		item: '',
//		quantityToBill: ''
//	}
	var invoiceId = invoiceRecord.getValue({
		fieldId: 'id'
	});
	
	var invoiceLines = { invoiceLines: [] };
	
	retrieveinvoiceLinesInfo(invoiceRecord,invoiceLines);
	
	var billLineCount = vendorBillRecord.getLineCount('item');
	
	for (var i = 0; i < billLineCount; i++){
		
		vendorBillRecord.selectLine({
			sublistId: 'item',
			line: i
		});
		
		var billItem = vendorBillRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'item'
		});
		var billQuantity = vendorBillRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'quantity'
		});
		
		var index =  invoiceLines.invoiceLines.findIndex(function(element) { return element.indexLine == i;} );
    	    	
		if (billItem == invoiceLines.invoiceLines[index].item ){
			
			vendorBillRecord.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'quantity',
				value: invoiceLines.invoiceLines[index].quantityToBill
			});			
		}	
		vendorBillRecord.commitLine({
			sublistId: 'item'
		});
	}

	vendorBillRecord.setValue({
		fieldId: 'custbody_bwp_invoice_reference',
		value: invoiceId
	});
	
	var billId = vendorBillRecord.save();
	
	// Eric Moulin - 24/03/2022 - Begin
	// var vendorBillRecord = RECORDMODULE.load({
	// 	type: RECORDMODULE.Type.VENDOR_BILL,
	// 	id: billId,
	// 	isDynamic: true,		
	// });

	// // // set original form used befor the first save
	// // vendorBillRecord.setValue({
	// // 	fieldId: 'customform',
	// // 	value: customFormUsed
	// // });

	// var billTransactionNumber = vendorBillRecord.getValue({
	// 	fieldId: 'transactionnumber'
	// });	
	// vendorBillRecord.setValue({
	// 	fieldId: 'tranid',
	// 	value: billTransactionNumber
	// });
	// vendorBillRecord.save();
	// Eric Moulin - 24/03/2022 - End
	
	logVar('invoiceId', invoiceId);
	var invoiceRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.INVOICE,
		id: invoiceId,
		isDynamic: true,		
	});
	logVar('billId', billId);
	invoiceRecord.setValue({
		fieldId: 'custbody_bwp_vendorbill_reference',
		value: billId
	});
	
	invoiceRecord.save();
	
	
}

function retrieveinvoiceLinesInfo(invoiceRecord,invoiceLines){	
	var lineCountInvoice = invoiceRecord.getLineCount('item');

	logVar('lineCountInvoice',lineCountInvoice);
	for (var i = 0; i < lineCountInvoice; i++)
	{	
		var invoiceQuantityLine = invoiceRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'quantity',
			line: i
		});
		
		var invoiceItemLine = invoiceRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'item',
			line: i
		});
		
		invoiceLines.invoiceLines.push({indexLine: i, item: invoiceItemLine, quantityToBill: invoiceQuantityLine});	
	}
}



function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}